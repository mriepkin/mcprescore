from flask import Flask
import pyodbc
from redis_cache import RedisCachingSystem
from redis_config import RedisConfig
from prescore_get_data import PrescoreGetData


def get_redis_client():
    return RedisCachingSystem(RedisConfig('localhost', 6379))

def get_sql_connection():
    connection = pyodbc.connect(
        'DRIVER={ODBC Driver 17 for SQL Server};SERVER=' + '10.11.0.74' + ';DATABASE=' + 'tkLender' + ';UID=' + 'm.riepkin' + ';PWD=' + 'iCB740WPynjYby4r')
    return connection


app = Flask(__name__)
connection = None
redis: RedisCachingSystem = get_redis_client()
loans_statuses_collection = None

@app.before_request
def before_request():
    global connection, redis, loans_statuses_collection
    connection = get_sql_connection()


@app.teardown_request
def teardown_request(exception):
    pass


@app.route('/get-loans', methods=['GET'])
def get_loans():
    return PrescoreGetData.execute(connection, redis)

if __name__ == '__main__':
    app.run(use_reloader=True, port=12000, threaded=False)

