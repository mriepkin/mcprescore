from redis_cache import RedisCachingSystem
import pandas as pd
import json
import time
from datetime import datetime
import config


class PrescoreGetData:
    __query = """SELECT
                l.Id LoanID,
                getdate() ConsiderationDate
            from loans l with (nolock)
            join Customers c  with (nolock) on l.Customer_Id = c.Id 
            join LoanDecisions ld with (nolock) on l.Decision_Id = ld.Id
            CROSS APPLY
            (SELECT count(*) as cnt FROM CreditBureauRequests cbr_in where cbr_in.CustomerId = c.Id and cbr_in.CreationDate > DATEADD(DD, -60, GETDATE())) cbr
            where
                l.[Status] = 4 and
                datediff(second, l.ToWaitForApproveDate, getdate()) > 60 and
                ld.[Source] = 0 and
                c.IsTester = 0  and
                c.IsBlackListed = 0 AND
                cbr.cnt > 0
            """

    @staticmethod
    def execute(sql_connection, cache: RedisCachingSystem):
        print('Request to get data at ' + str(datetime.fromtimestamp(time.time())))
        ltp = pd.read_sql(PrescoreGetData.__query, sql_connection)
        string_loans = ltp.to_json(orient="records")
        parsed_loans = json.loads(string_loans)
        print('Found loans: ' + str(len(parsed_loans)))
        loans_from_cache = cache.get(config.LOANS_CACHE_COLLECTION_NAME)
        PrescoreGetData.__delete_processed_loans_from_cache(cache)
        for loan in parsed_loans:
            if PrescoreGetData.__process_loan(cache, loan, loans_from_cache):
                print('Given to prescore ' + str(loan))
                return {'loan': loan}
        print('No loans to give')
        return {'loan': None}

    @staticmethod
    def __process_loan(cache: RedisCachingSystem, loan, loans_from_cache):
        loan_from_cache = loans_from_cache[str(loan['LoanID'])] if loans_from_cache is not None and str(loan['LoanID']) in loans_from_cache else None
        if loan_from_cache is None:
            return PrescoreGetData.__handle_new_loan(loans_from_cache, loan, cache)
        if loan_from_cache['IsFailReported'] or \
                (loan_from_cache['InProgress'] and time.time() - loan_from_cache['StartProcessTime'] < config.LOAN_MAX_EXECUTION_TIME):
            return PrescoreGetData.__handle_loan_in_progress(loans_from_cache, loan_from_cache, cache)
        if loan_from_cache['Retries'] >= config.MAX_LOAN_RETRIES:
            return PrescoreGetData.__handle_loan_fail(loans_from_cache, loan_from_cache, cache)
        return PrescoreGetData.__handle_loan_retry(loans_from_cache, loan_from_cache, cache)

    @staticmethod
    def __handle_new_loan(loans_from_cache, loan, cache):
        loan_to_insert = loan.copy()
        loan_to_insert['StartProcessTime'] = time.time()
        loan_to_insert['Retries'] = 0
        loan_to_insert['IsFailReported'] = False
        loan_to_insert['InProgress'] = True
        PrescoreGetData.__update_dict(loans_from_cache, loan_to_insert, cache)
        return True

    @staticmethod
    def __handle_loan_in_progress(loans_from_cache, loan, cache):
        return False

    @staticmethod
    def __handle_loan_fail(loans_from_cache, loan, cache):
        loan['InProgress'] = False
        loan['IsFailReported'] = True
        PrescoreGetData.__update_dict(loans_from_cache, loan, cache)
        return False

    @staticmethod
    def __handle_loan_retry(loans_from_cache, loan, cache):
        loan['InProgress'] = True
        loan['Retries'] += 1
        loan['StartProcessTime'] = time.time()
        PrescoreGetData.__update_dict(loans_from_cache, loan, cache)
        return True

    @staticmethod
    def __delete_processed_loans_from_cache(cache):
        loans_from_cache = cache.get(config.LOANS_CACHE_COLLECTION_NAME)
        if loans_from_cache is None:
            return
        to_delete = []
        for loan_id in loans_from_cache.keys():
            loan = loans_from_cache[loan_id]
            if time.time() - loan['StartProcessTime'] > config.LOAN_EXPIRE_TIME:
                to_delete.append(loan)
        print('Number of loans in cache before delete: ' + str(len(loans_from_cache.keys())))
        for loan in to_delete:
            del loans_from_cache[str(loan['LoanID'])]

        cache.delete(config.LOANS_CACHE_COLLECTION_NAME)
        cache.put(config.LOANS_CACHE_COLLECTION_NAME, loans_from_cache)
        print('Number of loans in cache after delete: ' + str(len(loans_from_cache.keys())))

    @staticmethod
    def __update_dict(loans_from_cache, loan, cache):
        if loans_from_cache is None:
            loans_from_cache = dict()
        if str(loan['LoanID']) in loans_from_cache.keys():
            del loans_from_cache[str(loan['LoanID'])]
        loans_from_cache[loan['LoanID']] = loan
        cache.delete(config.LOANS_CACHE_COLLECTION_NAME)
        cache.put(config.LOANS_CACHE_COLLECTION_NAME, loans_from_cache)


